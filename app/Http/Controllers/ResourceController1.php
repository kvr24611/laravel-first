<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreResource1Request;
use App\Http\Requests\UpdateResource1Request;
use App\Models\Resource1;
use App\Models\Resource2;

class ResourceController1 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct(Resource1 $resource1, Resource2 $resource2) {
        $this->middleware('auth');
        $this->middleware('onlymiddleware')->only('index');
        $this->middleware('exceptmiddleware')->except('destroy');

        parent::__construct($resource1);
        $this->resource2 = $resource2;
        
    }

     public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreResource1Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreResource1Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Resource1  $resource1
     * @return \Illuminate\Http\Response
     */
    public function show(Resource1 $resource1)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Resource1  $resource1
     * @return \Illuminate\Http\Response
     */
    public function edit(Resource1 $resource1)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateResource1Request  $request
     * @param  \App\Models\Resource1  $resource1
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateResource1Request $request, Resource1 $resource1)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Resource1  $resource1
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resource1 $resource1)
    {
        //
    }
}
