<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('log')->only('index');
        $this->middleware('can')->except('edit');
        
    }
    
    public function index() {
        dd('here');
    }

    public function edit($id) {
        dd($id,"edit(testpath/id/edit)");
    }

    // public function destroy(Request $request,$id) {
    //     dd($request->id."delete(testpath/id)");
    // }
}
