<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreResource2Request;
use App\Http\Requests\UpdateResource2Request;
use App\Models\Resource2;

class ResourceController2 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('onlymiddleware')->only('index');
        $this->middleware('exceptmiddleware')->except('destroy');
        
    }
    
     public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreResource2Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreResource2Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Resource2  $resource2
     * @return \Illuminate\Http\Response
     */
    public function show(Resource2 $resource2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Resource2  $resource2
     * @return \Illuminate\Http\Response
     */
    public function edit(Resource2 $resource2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateResource2Request  $request
     * @param  \App\Models\Resource2  $resource2
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateResource2Request $request, Resource2 $resource2)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Resource2  $resource2
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resource2 $resource2)
    {
        //
    }
}
