<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::resource('user','UserController');
// Route::resource('testpath','TestController');
// Route::get('test','TestController@show')->middleware('auth');


//assignment1

//question1
Route::get('user/{name}', 'UserController@show');

//question2
Route::group(['middleware'=>'costum'] , function() {
    Route::get('test','UserController@index');    
});

//question3
Route::resource('category','CategoryController');

// question4
Route::group(['prefix'=>'product'], function() {
    Route::resource('category','CategoryController');
});

//assignment2

Route::resources([
    'resource1' => 'ResourceController1',
    'resource2' => 'ResourceController2'
]);